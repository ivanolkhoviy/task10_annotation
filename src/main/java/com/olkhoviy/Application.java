package com.olkhoviy;

import com.olkhoviy.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().start();
    }
}
