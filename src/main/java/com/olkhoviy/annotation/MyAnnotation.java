package com.olkhoviy.annotation;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
public @interface MyAnnotation {
        String name() default "unknown";
        int age() default 18;
}
